#include<bits/stdc++.h>


using namespace std;

struct Kala {
   
    int code;
    string name;
    long int gheymat;
};

struct linkedlist {
   
    struct linkedlist* prev;
    int code;
    string keshvar;
    struct linkedlist* next;
    list<struct Kala> kalaha;
};


struct linkedlist* HEAD = NULL;

int isEmpty() {

    if(HEAD == NULL)
        return 1;
    return 0;
}

void insertKeshvar(int code, string name) {

    if(isEmpty() == 1) {
    	
    	HEAD = new struct linkedlist();
        HEAD->prev = NULL;
        HEAD->code = code;
        HEAD->keshvar = name;
        HEAD->next = NULL;

        cout << "keshvare " << HEAD->keshvar << " ba code " << HEAD->code << " ba movafaghiat afzoodeh shod!\n";
    }
    else {

        struct linkedlist* cur = HEAD;
        struct linkedlist* cur2;
        while(cur->next != NULL) {
            cur = cur->next;
        }

        cur->next = new struct linkedlist();
        cur2 = cur;
        cur = cur -> next;
        
        cur->prev = cur2;
        cur->code = code;
        cur->keshvar = name;
        cur->next = NULL;

        cout << "keshvare " << cur->keshvar << " ba code " << cur->code << " ba movafaghiat afzoodeh shod!\n";
    }
}

int find(string name) {

    if(isEmpty() == 1)
        return -1;
    
    struct linkedlist* cur = HEAD;
    while(cur != NULL) {

        if(cur->keshvar == name) {
        	return 1;
		}
        cur = cur->next;     
    }
    return -1;
}

void insertKala(int code_kala, string name_kala, string name_keshvar, long int gheymat_kala) {

    if(isEmpty() == 1 || find(name_keshvar) == -1) {

        cout << "Keshvare vared shodeh vojood nadarad ya liste keshvar ha khali ast\n";
    }
    else {

        struct linkedlist* cur = HEAD;
        while(cur->keshvar != name_keshvar) {
            cur = cur->next;
        }
        
        struct Kala k;
        k.code = code_kala;
        k.name = name_kala;
        k.gheymat = gheymat_kala;
        cur->kalaha.push_back(k);

        cout << "kalaye  " << k.name << " ba code " << k.code << " be keshvareh " <<
        name_keshvar << " ba movafaghiat afzoodeh shod!\n";
    }
}


void delete_keshvar(string name_keshvar) {
	
	if(isEmpty() == 1 || find(name_keshvar) == -1) {
	    cout << "Keshvare vared shodeh vojood nadarad\n";
		return;
	}

    struct linkedlist* cur = HEAD;

    if(HEAD->keshvar == name_keshvar) {

        HEAD = HEAD->next;
        HEAD->prev = NULL;
        free(cur);
        cout << "keshvare " << name_keshvar << " ba movafaghiat hazf shod!\n";
        return;
    }

    struct linkedlist* cur2;
    while(cur->keshvar != name_keshvar) {
        cur2 = cur;
        cur = cur->next;
    }
    cur2->next = cur->next;

    if(cur->next != NULL) {
        cur->next->prev = cur2;
    }

    free(cur);
    cout << "keshvare " << name_keshvar << " ba movafaghiat hazf shod!\n";
    return;
}


void delete_kala(string name_keshvar, string name_kala) {
	
	if(isEmpty() == 1 || find(name_keshvar) == -1) {
	    cout << "Keshvare vared shodeh vojood nadarad\n";
		return;
	}

     struct linkedlist* cur = HEAD;

    while(cur->keshvar != name_keshvar) {
        cur = cur->next;
    }
    
    list<Kala>::iterator it;
    list<Kala> k = cur->kalaha;
    bool flag = false;
    for(it = k.begin(); it != k.end(); it++) {
        if(it->name == name_keshvar) {
            k.erase(it);
            flag = true;
        }
    }
    
    if(flag) {
        cout << "kalaye " << name_kala << " ba movafaghiat hazf shod!\n";
    }
    else {
        cout << "kalaye " << name_kala << " dar keshvare " << name_keshvar << " vojood nadard\n";
    }
    
    return;
}

void showKala(string name_keshvar) {
    
    if(isEmpty() == 1 || find(name_keshvar) == -1) {

        cout << "Keshvare vared shodeh vojood nadarad ya liste keshvar ha khali ast\n";
    }
    else {
        
        struct linkedlist* cur = HEAD;
        while(cur->keshvar != name_keshvar) {
            cur = cur->next;
        }
        
    list<Kala>::iterator it;
    cout << "keshvar" << " \t\t " << "gheymat\n\n";
    
    for(it = cur->kalaha.begin(); it != cur->kalaha.end(); it++) {
        cout << it->name << " \t\t " << it->gheymat << endl;
        }
    }
}

void keshvarKala(string name_kala) {
    
    struct linkedlist* cur = HEAD;
    list<Kala>::iterator it;
    bool flag = false;
    
    while(cur != NULL) {
        
        for(it = cur->kalaha.begin(); it != cur->kalaha.end(); it++) {
            if(it->name == name_kala) {
                if(flag == false) {
                    cout << "keshvar" << " \t\t " << "gheymat\n\n";
                }
                cout << cur->keshvar << " \t\t " << it->gheymat << endl;
                flag = true;
                break;
            }
        }
        cur = cur->next;
    }
    
    if(flag == false)   
        cout << "hich keshvari chenin kalayi nadarad\n";
}

void maximumVaredat() {
    
    struct linkedlist* cur = HEAD;
    long int maxGheymat = 0;
    long int temp = 0;
    string keshvarMax = "";
    
    list<Kala>::iterator it;
    bool flag = false;
    
    while(cur != NULL) {
        
        for(it = cur->kalaha.begin(); it != cur->kalaha.end(); it++) {
            temp += it->gheymat;
        }
        if(temp > maxGheymat) {
            maxGheymat = temp;
            keshvarMax = cur->keshvar;
        }
        temp = 0;
        cur = cur->next;
    }
    
    cout << "keshvareh " << keshvarMax << " ba mablaghe " << maxGheymat << " dollar bishtarin varedat ra deshteh ast!\n";
}


int main()
{
    bool darkhast = true;
    while(darkhast) {
    
    cout << "---------------------------------------\n";
    cout << "Adade amaliate morede nazar ra vared konid :\n";
    cout << "[1]-\t Afzoodane Keshvar\n";
    cout << "[2]-\t Afzoodane Kala be Keshvar\n";
    cout << "[3]-\t Hazfe Keshvar\n";
    cout << "[4]-\t Hazfe kala az Keshvar\n";
    cout << "[5]-\t Namayesh kalahaye varedati yek keshavr\n";
    cout << "[6]-\t Namayeshe keshvar haye vared konandeh yek kala\n";
    cout << "[7]-\t Namayeshe keshvare ba bishtarin varedat\n";
    cout << "---------------------------------------\n";
    
    int n;
    cin >> n;
    
    if(n == 1) {
            int code;
            cout << "code keshvare morede nazar ra vared konid :\n";
            cin >> code;
            string name;
            cout << "esme keshvare morede nazar ra vared konid :\n";
            cin >> name;
            insertKeshvar(code, name);
            }
            else if(n == 2) {
            int code_kala;
            cout << "code kalaye morede nazar ra vared konid :\n";
            cin >> code_kala;
            string name_kala;
            cout << "name kalaye morede nazar ra vared konid :\n";
            cin >> name_kala;
            string name_keshvar;
            cout << "esme keshvare morede nazar ra vared konid :\n";
            cin >> name_keshvar;
            long int gheymat_kala;
            cout << "gheymate kalaye morede nazar ra vared konid :\n";
            cin >> gheymat_kala;
            insertKala(code_kala, name_kala, name_keshvar, gheymat_kala);
            }
        	else if(n == 3) {
            string name_keshvar;
            cout << "esme keshvare morede nazar ra vared konid :\n";
            cin >> name_keshvar;
            delete_keshvar(name_keshvar);
            }
        	else if(n == 4) {
            string name_keshvar;
            cout << "esme keshvare morede nazar ra vared konid :\n";
            cin >> name_keshvar;
            string name_kala;
            cout << "name kalaye morede nazar ra vared konid :\n";
            cin >> name_kala;
            delete_kala(name_keshvar, name_kala);
            }
        	else if(n == 5) {
            string name_keshvar;
            cout << "esme keshvare morede nazar ra vared konid :\n";
            cin >> name_keshvar;
            showKala(name_keshvar);
            }
        	else if(n == 6) {
            string name_kala;
            cout << "name kalaye morede nazar ra vared konid :\n";
            cin >> name_kala;
            keshvarKala(name_kala);
            }
        	else if(n == 7) {
            maximumVaredat();
            }
            
            cout << "---------------------------------------\n";
    
    		cout << "Aya Darkhaste diagri darid?\n";
    		cout << "[1] Bale\n";
    		cout << "[2] Kheir\n";
    		int c;
    		cin >> c;
    		if(c == 2) 
        		darkhast = false;
    		}
    
   




	return 0;
}
